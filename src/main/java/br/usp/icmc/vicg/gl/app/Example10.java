package br.usp.icmc.vicg.gl.app;

import br.usp.icmc.vicg.gl.core.Light;
import br.usp.icmc.vicg.gl.core.Material;
import br.usp.icmc.vicg.gl.matrix.Matrix4;
import br.usp.icmc.vicg.gl.model.Cube;
import br.usp.icmc.vicg.gl.model.SimpleModel;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.media.opengl.GL;
import javax.media.opengl.GL3;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;

import br.usp.icmc.vicg.gl.util.Shader;
import br.usp.icmc.vicg.gl.util.ShaderFactory;
import br.usp.icmc.vicg.gl.util.ShaderFactory.ShaderType;

import com.jogamp.opengl.util.AnimatorBase;
import com.jogamp.opengl.util.FPSAnimator;

public class Example10 implements GLEventListener {

  private final Shader shader; // Gerenciador dos shaders
  private final Matrix4 modelMatrix;
  private final Matrix4 projectionMatrix;
  private final Matrix4 viewMatrix;
  private final Material material;
  private final SimpleModel[] cubes;
  private final Light light;
  private final float[] positions;
  private final float[] positions2;
  private final float[] colors;
  private final float[] scales;
  private float[] rotations;
  private float amp, phase, time;

  public Example10() {
    // Carrega os shaders
    shader = ShaderFactory.getInstance(ShaderType.COMPLETE_SHADER);
    modelMatrix = new Matrix4();
    projectionMatrix = new Matrix4();
    viewMatrix = new Matrix4();
    material = new Material();
    light = new Light();
    
    cubes = new Cube[9];
    for (int i = 0; i < cubes.length; ++i) {
        cubes[i] = new Cube();
    }
    
    positions = new float[] {
        0, -.5f, 0, // base
        -.5f, 0, 0, // ombro-esq
        .5f, 0, 0,  // ombro-dir
        -.5f, -1.5f, 0, // braço-esq
        .5f, -1.5f, 0, // braço-esq
        0, 1.5f, 0, // tronco
        0, 1, 0, // cabeça
        0, -2, 0, // perna-esq
        0, -2, 0 // perna-dir
    };
    
    positions2 = new float[] {
        0, 0, 0, // base
        -1, 7, 0, // ombro-esq
        1, 7, 0, // ombro-dir
        -1, 7, 0, // braço-esq
        1, 7, 0, // braço-dir
        0, 4, 0, // tronco
        0, 7, 0, // cabeça
        -.5f, 4, 0, // perna-esq
        .5f, 4, 0 // perna-dir
    };
    
    scales = new float[] {
        16, 1, 16, // base
        1, 1, 1, // ombro-esq
        1, 1, 1, // ombro-dir
        1, 2, 1, // braço-esq
        1, 2, 1, // braço-dir
        2, 3, 1, // tronco
        2.125f, 2, 2.125f, // cabeça
        1, 4, 1, // perna-esq
        1, 4, 1 // perna-dir
    };
    
    colors = new float[] {
        0.0f, 0.6f, 0.0f, 1.0f, // base
        0.0f, 0.9f, 0.9f, 1.0f, // ombro-esq
        0.0f, 0.9f, 0.9f, 1.0f, // ombro-dir
        1.0f, 0.7f, 0.6f, 1.0f, // braço-esq
        1.0f, 0.7f, 0.6f, 1.0f, // braço-dir
        0.0f, 0.9f, 0.9f, 1.0f, // tronco
        1.0f, 0.7f, 0.6f, 1.0f, // cabeça
        0.1f, 0.1f, 0.7f, 1.0f, // perna-esq
        0.1f, 0.1f, 0.7f, 1.0f // perna-dir
    };
    
    amp = 30;
    phase = 1;
    time = 0;
    
    rotations = new float[] {
        0.0f, 1.0f, 0.0f, 0.0f, // base
        -amp * phase, 1.0f, 0.0f, 0.0f, // ombro-esq
        amp * phase, 1.0f, 0.0f, 0.0f, // ombro-dir
        -amp * phase, 1.0f, 0.0f, 0.0f, // braço-esq
        amp * phase, 1.0f, 0.0f, 0.0f, // braço-dir
        0.0f, 1.0f, 0.0f, 0.0f, // tronco
        0.0f, 1.0f, 0.0f, 0.0f, // cabeça
        amp * phase, 1.0f, 0.0f, 0.0f, // perna=esq
        -amp * phase, 1.0f, 0.0f, 0.0f // perna-dir
    };
  }

  @Override
  public void init(GLAutoDrawable drawable) {
    // Get pipeline
    GL3 gl = drawable.getGL().getGL3();

    // Print OpenGL version
    System.out.println("OpenGL Version: " + gl.glGetString(GL.GL_VERSION) + "\n");

    gl.glClearColor(0, 0, 0, 0);
    gl.glClearDepth(1.0f);

    gl.glEnable(GL.GL_DEPTH_TEST);
    gl.glEnable(GL.GL_CULL_FACE);

    //inicializa os shaders
    shader.init(gl);

    //ativa os shaders
    shader.bind();

    //inicializa a matrix Model and Projection
    modelMatrix.init(gl, shader.getUniformLocation("u_modelMatrix"));
    projectionMatrix.init(gl, shader.getUniformLocation("u_projectionMatrix"));
    viewMatrix.init(gl, shader.getUniformLocation("u_viewMatrix"));

    // Inicializa o sistema de coordenadas
    projectionMatrix.loadIdentity();
    projectionMatrix.ortho(
            -2, 2,
            -2, 2,
            -10, 10);
    projectionMatrix.bind();

    viewMatrix.loadIdentity();
    viewMatrix.lookAt(
            1, 1, 1,
            0, 0, 0,
            0, 1, 0);
    viewMatrix.bind();

    light.init(gl, shader);
    light.setPosition(new float[]{0.0f, 1.0f, 2.0f, 0.0f});
    light.setAmbientColor(new float[]{0.9f, 0.9f, 0.9f, 0.0f});
    light.setDiffuseColor(new float[]{1.0f, 1.0f, 1.0f, 0.0f});
    light.setSpecularColor(new float[]{0.9f, 0.9f, 0.9f, 0.0f});
    light.bind();
    
    material.init(gl, shader);
    material.setAmbientColor(new float[]{0.5f, 0.5f, 0.5f, 0.0f});
    material.setDiffuseColor(new float[]{1.0f, 1.0f, 1.0f, 0.0f});
    material.setSpecularColor(new float[]{0.9f, 0.9f, 0.9f, 0.0f});
    material.setSpecularExponent(32);
    material.bind();

    //cria o objeto a ser desenhado
    for (int i = 0; i < cubes.length; ++i) {
        cubes[i].setColors(new float[]{colors[4*i], colors[4*i+1], colors[4*i+2], colors[4*i+3]});
        cubes[i].init(gl, shader);
    }
  }

  @Override
  public void display(GLAutoDrawable drawable) {
    // Recupera o pipeline
    GL3 gl = drawable.getGL().getGL3();

    // Limpa o frame buffer com a cor definida
    gl.glClear(GL3.GL_COLOR_BUFFER_BIT | GL3.GL_DEPTH_BUFFER_BIT);

    for (int i = 0; i < cubes.length; ++i) {
        modelMatrix.loadIdentity();
        modelMatrix.translate(positions2[3*i]*0.125f, positions2[3*i+1]*0.125f, positions2[3*i+2]*0.125f);
        modelMatrix.rotate(rotations[4*i], rotations[4*i+1], rotations[4*i+2], rotations[4*i+3]);
        modelMatrix.translate(positions[3*i]*0.125f, positions[3*i+1]*0.125f, positions[3*i+2]*0.125f);
        modelMatrix.scale(scales[3*i]*0.125f, scales[3*i+1]*0.125f, scales[3*i+2]*0.125f);
        modelMatrix.bind();
        
        cubes[i].bind();
        cubes[i].draw();
    }

    // Força execução das operações declaradas
    gl.glFlush();
    
    time += 0.166666666f;
    phase = (float) Math.cos(time);
    
    rotations = new float[] {
        0.0f, 1.0f, 0.0f, 0.0f, // base
        -amp * phase, 1.0f, 0.0f, 0.0f, // ombro-esq
        amp * phase, 1.0f, 0.0f, 0.0f, // ombro-dir
        -amp * phase, 1.0f, 0.0f, 0.0f, // braço-esq
        amp * phase, 1.0f, 0.0f, 0.0f, // braço-dir
        0.0f, 1.0f, 0.0f, 0.0f, // tronco
        0.0f, 1.0f, 0.0f, 0.0f, // cabeça
        amp * phase, 1.0f, 0.0f, 0.0f, // perna=esq
        -amp * phase, 1.0f, 0.0f, 0.0f, // perna-dir
        0.0f, 1.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f, 0.0f
    };
  }

  @Override
  public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
  }

  @Override
  public void dispose(GLAutoDrawable drawable) {
    // Apaga o buffer
    for (SimpleModel c : cubes) {
        c.dispose();
    }
  }

  public static void main(String[] args) {
    // Get GL3 profile (to work with OpenGL 4.0)
    GLProfile profile = GLProfile.get(GLProfile.GL3);

    // Configurations
    GLCapabilities glcaps = new GLCapabilities(profile);
    glcaps.setDoubleBuffered(true);
    glcaps.setHardwareAccelerated(true);

    // Create canvas
    GLCanvas glCanvas = new GLCanvas(glcaps);

    // Add listener to panel
    Example10 listener = new Example10();
    glCanvas.addGLEventListener(listener);

    Frame frame = new Frame("Example 05");
    frame.setSize(600, 600);
    frame.add(glCanvas);
    final AnimatorBase animator = new FPSAnimator(glCanvas, 60);

    frame.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        new Thread(new Runnable() {
          @Override
          public void run() {
            animator.stop();
            System.exit(0);
          }
        }).start();
      }
    });
    frame.setVisible(true);
    animator.start();
  }
}
