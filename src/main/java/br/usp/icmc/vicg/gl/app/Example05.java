package br.usp.icmc.vicg.gl.app;

import br.usp.icmc.vicg.gl.matrix.Matrix4;
import br.usp.icmc.vicg.gl.model.Circle;
import br.usp.icmc.vicg.gl.model.SimpleModel;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.media.opengl.GL;
import javax.media.opengl.GL3;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;

import br.usp.icmc.vicg.gl.util.Shader;
import br.usp.icmc.vicg.gl.util.ShaderFactory;
import br.usp.icmc.vicg.gl.util.ShaderFactory.ShaderType;

import com.jogamp.opengl.util.AnimatorBase;
import com.jogamp.opengl.util.FPSAnimator;

public class Example05 implements GLEventListener {

  private final Shader shader; // Gerenciador dos shaders
  private final Matrix4 modelMatrix;
  private final Matrix4 viewMatrix;
  private final Matrix4 projectionMatrix;
  private final SimpleModel circle, axis;
  private float time;

  public Example05() {
    // Carrega os shaders
    shader = ShaderFactory.getInstance(ShaderType.MODEL_PROJECTION_MATRIX_SHADER);
    modelMatrix = new Matrix4();
    viewMatrix = new Matrix4();
    projectionMatrix = new Matrix4();
    circle = new Circle();
    axis = new SimpleModel() {
        {
            vertex_buffer = new float[] {
                -1.0f, 0.0f, 0.0f,
                1.0f, 0.0f, 0.0f,
                0.0f, -1.0f, 0.0f,
                0.0f, 1.0f, 0.0f
            };
        }
        
        @Override
        public void draw() {
            draw(GL.GL_LINES);
        }
        
    };
  }

  @Override
  public void init(GLAutoDrawable drawable) {
    // Get pipeline
    GL3 gl = drawable.getGL().getGL3();

    // Print OpenGL version
    System.out.println("OpenGL Version: " + gl.glGetString(GL.GL_VERSION) + "\n");

    gl.glClearColor(0, 0, 0, 0);

    //inicializa os shaders
    shader.init(gl);

    //ativa os shaders
    shader.bind();

    //inicializa a matrix Model and Projection
    modelMatrix.init(gl, shader.getUniformLocation("u_modelMatrix"));
    projectionMatrix.init(gl, shader.getUniformLocation("u_projectionMatrix"));
    viewMatrix.init(gl, shader.getUniformLocation("u_viewMatrix"));
    
    projectionMatrix.loadIdentity();
    projectionMatrix.ortho(-2.0f, 2.0f, 
            -2.0f, 2.0f, 
            -2.0f, 2.0f);
    projectionMatrix.bind();
    projectionMatrix.print();

    viewMatrix.loadIdentity();
    viewMatrix.lookAt(0.0f, 0.0f, 2.0f,
            0.0f, 0.0f, 0.0f,
            0.0f, 1.0f, 0.0f);
    viewMatrix.bind();

    //cria o objeto a ser desenhado
    circle.init(gl, shader);
    axis.init(gl, shader);
    
    time = 0.0f;
  }

  @Override
  public void display(GLAutoDrawable drawable) {
    // Recupera o pipeline
    GL3 gl = drawable.getGL().getGL3();

    // Limpa o frame buffer com a cor definida
    gl.glClear(GL3.GL_COLOR_BUFFER_BIT);
    
    float radA = 0.25f;
    float radB = radA / 2.0f;
    float radC = radB / 2.0f;
    float tau = 6.2831853071795f;

    modelMatrix.loadIdentity();
    modelMatrix.translate(radA, radA, 0.0f);
    modelMatrix.scale(radA, radA, 1.0f);
    modelMatrix.bind();
    
    circle.bind();
    circle.draw(GL3.GL_LINE_LOOP);
    
    float phase = tau * time;
    modelMatrix.loadIdentity();
    modelMatrix.translate(radA, radA, 0.0f);
    modelMatrix.rotate(phase, 0.0f, 0.0f, 1.0f);
    modelMatrix.translate(radA+radB, 0.0f, 0.0f);
    modelMatrix.scale(radB, radB, 1.0f);
    modelMatrix.bind();
    
    circle.bind();
    circle.draw(GL3.GL_LINE_LOOP);
    
    float phase2 = 2.0f * tau * time;
    modelMatrix.loadIdentity();
    modelMatrix.translate(radA, radA, 0.0f);
    modelMatrix.rotate(phase, 0.0f, 0.0f, 1.0f);
    modelMatrix.translate(radA+radB, 0.0f, 0.0f);
    modelMatrix.rotate(phase2, 0.0f, 0.0f, 1.0f);
    modelMatrix.translate(radB+radC, 0.0f, 0.0f);
    modelMatrix.scale(radC, radC, 1.0f);
    modelMatrix.bind();
    
    circle.bind();
    circle.draw(GL3.GL_LINE_LOOP);
    
    modelMatrix.loadIdentity();
    modelMatrix.bind();
    
    axis.bind();
    axis.draw();

    // Força execução das operações declaradas
    gl.glFlush();
    
    time += 0.16;
  }

  @Override
  public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
    // Evita a divisão por zero
    if (height == 0) {
      height = 1;
    }

    // Inicializa o sistema de coordenadas
    projectionMatrix.loadIdentity();

    // Estabelece a janela de recorte
    if (width <= height) {
      projectionMatrix.ortho2D(
              -1.0f, 1.0f, 
              (-1.0f * height) / width, (1.0f * height) / width);
    } else {
      projectionMatrix.ortho2D(
              (-1.0f * width) / height, (1.0f * width) / height, 
              -1.0f, 1.0f);
    }
    
    projectionMatrix.bind();
  }

  @Override
  public void dispose(GLAutoDrawable drawable) {
    // Apaga o buffer
    circle.dispose();
  }

  public static void main(String[] args) {
    // Get GL3 profile (to work with OpenGL 4.0)
    GLProfile profile = GLProfile.get(GLProfile.GL3);

    // Configurations
    GLCapabilities glcaps = new GLCapabilities(profile);
    glcaps.setDoubleBuffered(true);
    glcaps.setHardwareAccelerated(true);

    // Create canvas
    GLCanvas glCanvas = new GLCanvas(glcaps);

    // Add listener to panel
    Example05 listener = new Example05();
    glCanvas.addGLEventListener(listener);

    Frame frame = new Frame("Example 05");
    frame.setSize(600, 600);
    frame.add(glCanvas);
    final AnimatorBase animator = new FPSAnimator(glCanvas, 60);

    frame.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        new Thread(new Runnable() {
          @Override
          public void run() {
            animator.stop();
            System.exit(0);
          }
        }).start();
      }
    });
    frame.setVisible(true);
    animator.start();
  }
}
